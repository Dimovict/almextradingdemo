require 'test_helper'

class CustomsAgentsControllerTest < ActionController::TestCase
  setup do
    @customs_agent = customs_agents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customs_agents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customs_agent" do
    assert_difference('CustomsAgent.count') do
      post :create, customs_agent: { email: @customs_agent.email, key: @customs_agent.key, last_name: @customs_agent.last_name, name: @customs_agent.name, phone: @customs_agent.phone }
    end

    assert_redirected_to customs_agent_path(assigns(:customs_agent))
  end

  test "should show customs_agent" do
    get :show, id: @customs_agent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customs_agent
    assert_response :success
  end

  test "should update customs_agent" do
    patch :update, id: @customs_agent, customs_agent: { email: @customs_agent.email, key: @customs_agent.key, last_name: @customs_agent.last_name, name: @customs_agent.name, phone: @customs_agent.phone }
    assert_redirected_to customs_agent_path(assigns(:customs_agent))
  end

  test "should destroy customs_agent" do
    assert_difference('CustomsAgent.count', -1) do
      delete :destroy, id: @customs_agent
    end

    assert_redirected_to customs_agents_path
  end
end
