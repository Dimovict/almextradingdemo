require 'test_helper'

class LegalRepresentativesControllerTest < ActionController::TestCase
  setup do
    @legal_representative = legal_representatives(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:legal_representatives)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create legal_representative" do
    assert_difference('LegalRepresentative.count') do
      post :create, legal_representative: { company_id: @legal_representative.company_id, email: @legal_representative.email, last_name: @legal_representative.last_name, name: @legal_representative.name, phone: @legal_representative.phone }
    end

    assert_redirected_to legal_representative_path(assigns(:legal_representative))
  end

  test "should show legal_representative" do
    get :show, id: @legal_representative
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @legal_representative
    assert_response :success
  end

  test "should update legal_representative" do
    patch :update, id: @legal_representative, legal_representative: { company_id: @legal_representative.company_id, email: @legal_representative.email, last_name: @legal_representative.last_name, name: @legal_representative.name, phone: @legal_representative.phone }
    assert_redirected_to legal_representative_path(assigns(:legal_representative))
  end

  test "should destroy legal_representative" do
    assert_difference('LegalRepresentative.count', -1) do
      delete :destroy, id: @legal_representative
    end

    assert_redirected_to legal_representatives_path
  end
end
