require 'ruby_parser'

# Monkey patch to support Ruby 2.2
RubyParser.class_eval do
  def self.for_current_ruby
    Ruby21Parser.new
  end
end