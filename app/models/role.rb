class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true

  validates :resource_type,
            :inclusion => { :in => Rolify.resource_types },
            :allow_nil => true

  scopify

  def role_label_method
    case self.name
    	when "admin"
    		"Administrador"
    	when "customs_agent"
    		"Agente Aduanal"
    	when "company_representative"
    		"Representante de empresa"
    	else
    		"Default"
    end
  end

  rails_admin do
    object_label_method do
      :role_label_method
    end

    visible false
  end
end
