class CustomsAgentsController < ApplicationController
  before_action :set_customs_agent, only: [:show, :edit, :update, :destroy]

  # GET /customs_agents
  # GET /customs_agents.json
  def index
    @customs_agents = CustomsAgent.all
  end

  # GET /customs_agents/1
  # GET /customs_agents/1.json
  def show
  end

  # GET /customs_agents/new
  def new
    @customs_agent = CustomsAgent.new
  end

  # GET /customs_agents/1/edit
  def edit
  end

  # POST /customs_agents
  # POST /customs_agents.json
  def create
    @customs_agent = CustomsAgent.new(customs_agent_params)

    respond_to do |format|
      if @customs_agent.save
        format.html { redirect_to @customs_agent, notice: 'Customs agent was successfully created.' }
        format.json { render :show, status: :created, location: @customs_agent }
      else
        format.html { render :new }
        format.json { render json: @customs_agent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customs_agents/1
  # PATCH/PUT /customs_agents/1.json
  def update
    respond_to do |format|
      if @customs_agent.update(customs_agent_params)
        format.html { redirect_to @customs_agent, notice: 'Customs agent was successfully updated.' }
        format.json { render :show, status: :ok, location: @customs_agent }
      else
        format.html { render :edit }
        format.json { render json: @customs_agent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customs_agents/1
  # DELETE /customs_agents/1.json
  def destroy
    @customs_agent.destroy
    respond_to do |format|
      format.html { redirect_to customs_agents_url, notice: 'Customs agent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customs_agent
      @customs_agent = CustomsAgent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customs_agent_params
      params.require(:customs_agent).permit(:name, :last_name, :key, :email, :phone)
    end
end
