class LegalRepresentativesController < ApplicationController
  before_action :set_legal_representative, only: [:show, :edit, :update, :destroy]

  # GET /legal_representatives
  # GET /legal_representatives.json
  def index
    @legal_representatives = LegalRepresentative.all
  end

  # GET /legal_representatives/1
  # GET /legal_representatives/1.json
  def show
  end

  # GET /legal_representatives/new
  def new
    @legal_representative = LegalRepresentative.new
  end

  # GET /legal_representatives/1/edit
  def edit
  end

  # POST /legal_representatives
  # POST /legal_representatives.json
  def create
    @legal_representative = LegalRepresentative.new(legal_representative_params)

    respond_to do |format|
      if @legal_representative.save
        format.html { redirect_to register_product_path, notice: 'Legal representative was successfully created.' }
        format.json { render :show, status: :created, location: @legal_representative }
      else
        format.html { render :new }
        format.json { render json: @legal_representative.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /legal_representatives/1
  # PATCH/PUT /legal_representatives/1.json
  def update
    respond_to do |format|
      if @legal_representative.update(legal_representative_params)
        format.html { redirect_to @legal_representative, notice: 'Legal representative was successfully updated.' }
        format.json { render :show, status: :ok, location: @legal_representative }
      else
        format.html { render :edit }
        format.json { render json: @legal_representative.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /legal_representatives/1
  # DELETE /legal_representatives/1.json
  def destroy
    @legal_representative.destroy
    respond_to do |format|
      format.html { redirect_to legal_representatives_url, notice: 'Legal representative was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_legal_representative
      @legal_representative = LegalRepresentative.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def legal_representative_params
      params.require(:legal_representative).permit(:name, :last_name, :phone, :email, :company_id)
    end
end
