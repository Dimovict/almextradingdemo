json.array!(@customs_agents) do |customs_agent|
  json.extract! customs_agent, :id, :name, :last_name, :key, :email, :phone
  json.url customs_agent_url(customs_agent, format: :json)
end
