json.array!(@legal_representatives) do |legal_representative|
  json.extract! legal_representative, :id, :name, :last_name, :phone, :email, :company_id
  json.url legal_representative_url(legal_representative, format: :json)
end
