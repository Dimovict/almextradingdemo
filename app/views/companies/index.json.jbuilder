json.array!(@companies) do |company|
  json.extract! company, :id, :name, :RFC, :billing_address, :shipping_address
  json.url company_url(company, format: :json)
end
