json.array!(@products) do |product|
  json.extract! product, :id, :name, :description, :height, :width, :length, :weight, :special_considerations, :company_id, :origin_certificate, :shipment_type, :shipping_address, :return_to_address, :quantity
  json.url product_url(product, format: :json)
end
