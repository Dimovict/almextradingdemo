class CreateLegalRepresentatives < ActiveRecord::Migration
  def change
    create_table :legal_representatives do |t|
      t.string :name
      t.string :last_name
      t.string :phone
      t.string :email
      t.belongs_to :company, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
