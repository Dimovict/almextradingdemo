class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :RFC
      t.string :billing_address
      t.string :shipping_address

      t.timestamps null: false
    end
  end
end
