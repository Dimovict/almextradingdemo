class CreateCustomsAgents < ActiveRecord::Migration
  def change
    create_table :customs_agents do |t|
      t.string :name
      t.string :last_name
      t.string :key
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
  end
end
