class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.float :height
      t.float :width
      t.float :length
      t.float :weight
      t.text :special_considerations
      t.belongs_to :company, index: true, foreign_key: true
      t.binary :origin_certificate
      t.string :filename
      t.string :content_type
      t.string :shipment_type
      t.string :shipping_address
      t.string :return_to_address
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
